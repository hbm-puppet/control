require 'yaml'

# vm defaults
default_box = 'centos/7'
default_memory = 1024
default_cpus = 1
default_role = 'default'
default_domain = 'mshome.net'
default_puppet_apply = true

# get the path to the repo
basedir = File.dirname(__FILE__)

# get the users' ssh public key
if File.file?("#{Dir.home}/.ssh/id_rsa.pub")
  ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
end

# get the SSH CA public key
if File.file?("#{basedir}/files/vagrant-sshca.pub")
  sshca_pub_key = File.readlines("#{basedir}/files/vagrant-sshca.pub").first.strip
end

# load the vagrant VM file
vms = YAML.load_file("#{basedir}/files/vagrant-vms.yaml")

# define the VMs
Vagrant.configure('2') do |config|
  vms.each do |vmname, vmsettings|
    config.vm.define vmname do |this|

      # get VM settings
      role   = vmsettings['role'] || default_role
      box    = vmsettings['box'] || default_box
      memory = vmsettings['memory'] || default_memory
      cpus   = vmsettings['cpus'] || default_cpus

      # work out whether to run puppet apply or not
      if vmsettings.key?('apply')
        puppet_apply = vmsettings['apply']
      else
        puppet_apply = default_puppet_apply
      end

      # configure the VM
      this.vm.hostname = "#{vmname}.#{default_domain}"
      this.vm.box = box
      this.vm.provider 'hyperv' do |hv|
        hv.vmname = "#{vmname}.#{default_domain}"
        hv.memory = memory
        hv.cpus = cpus
        hv.vm_integration_services = {
          guest_service_interface: true,
          heartbeat: true,
          key_value_pair_exchange: true,
          shutdown: true,
          time_synchronization: true,
          vss: true
        }
      end

      # configure networking
      this.vm.network :private_network, type: 'dhcp'

      # create /vagrant using rsync
      this.vm.synced_folder '.', '/vagrant', type: 'rsync'

      # provision VM
      this.vm.provision :shell,
         :path => 'files/vagrant-provision.sh',
         :env  => {
           'ROLE'          => role,
           'SSH_PUB_KEY'   => ssh_pub_key,
           'SSHCA_PUB_KEY' => sshca_pub_key,
           'PUPPET_APPLY'  => puppet_apply
         }

    end
  end
end
