#!/bin/sh

# Work out the base directory of the repo
FILES=$(dirname $0)
BASE=$(dirname $FILES)

# Use vagrant as the default platform
PLATFORM=$1
if [ "$PLATFORM" == "" ]
then
  PLATFORM="vagrant"
fi

# use vagrant as the default tier
TIER="live"
case "$PLATFORM" in
  vagrant)   TIER="vagrant";;
  openstack) TIER="test";;
esac

for REPONAME in control ca enc
do

  # search the platform data for the fqdn of the deploy server 
  PLATFORM_PATH="$BASE/data/platform/$PLATFORM"
  SERVER_VARIABLE="profiles::puppet::deploy_server"
  SERVER=$(awk "/^${SERVER_VARIABLE}:/ { print \$2 }" "${PLATFORM_PATH}.yaml" 2> /dev/null)

  # search the puppetdeploy role for the git url
  PUPPETDEPLOY_PATH="$BASE/data/role/puppetdeploy"
  REPO_VARIABLE="profiles::puppet::deploy::${REPONAME}_repo"
  REPO=$(awk "/^${REPO_VARIABLE}:/ { print \$2 }" "${PUPPETDEPLOY_PATH}.yaml" 2> /dev/null)
  # override with the tier-specific value if it exists
  TIER_REPO=$(awk "/^${REPO_VARIABLE}:/ { print \$2 }" "${PUPPETDEPLOY_PATH}/${TIER}.yaml" 2> /dev/null)
  if [ "$TIER_REPO" != "" ]
  then
    REPO=$TIER_REPO
  fi

  # exit if the server was not found
  if [ "$SERVER" == "" ]
  then
    echo "Error: could not find deploy server for $REPONAME"
    exit 1
  fi

  # exit if the repo was not found
  if [ "$REPO" == "" ]
  then
    echo "Error: could not find repo for $REPONAME"
    exit 1
  fi

  echo "triggering pipeline on $SERVER for $REPONAME in $TIER"
  curl -s -k "https://${SERVER}/git/notifyCommit?url=${REPO}" | grep ^Scheduled

done
