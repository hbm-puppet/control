BEGIN {
  print "profiles::certs::private_keys:"
  print "  " cn ":"
  print "    content: >";
}
/^-----BEGIN PKCS7-----/ {
  print "      ENC[PKCS7,";
  next;
}
/^-----END PKCS7-----/ {
  print "      ]";
  next;
}
{
  print "      " $0;
}
END {
  print "";
}