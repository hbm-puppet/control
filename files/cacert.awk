BEGIN {
  print "profiles::certs::ca_certs:"
  print "  " cn ":"
  print "    content: |";
}
/^-----BEGIN CERTIFICATE-----/ {
  preamble = "false"
}
{
  if (preamble == "false") {
    print "      " $0;
  }
}
END {
  print "";
}