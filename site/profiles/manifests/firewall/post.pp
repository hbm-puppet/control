# == Class: profiles::firewall::post
#
# Drops all traffic not covered by other standard and customised
# rules.
#

class profiles::firewall::post {

  # avoid dependency cycles
  Firewall {
    before => undef
  }

  # drop all other traffic
  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop'
  }
  firewall { '999 drop all (IPv6)':
    proto    => 'all',
    action   => 'drop',
    provider => 'ip6tables'
  }

}
