# == Defined Type: profiles::firewall::rule
#
# Allow traffic to a set of TCP/UDP ports optionally from a list
# of IP addresses or IP address ranges.
#
# === Parameters
#
# [*name*]
#   (Namevar) Friendly name of the service being opened.
#
# [*dport*]
#   (Required) Single port or an array of ports.
#
# [*proto*]
#   (Required) Protocol to use (tcp or udp).
#
# [*source*]
#   (Optional) List of IP addresses or ranges.
#

define profiles::firewall::rule (
  Variant[Integer, Array[Integer]]        $dport,
  String                                  $proto,
  Variant[Boolean, String, Array[String]] $source = false
)  {

  # default hash that contains the dport and protocol parameters
  $defaults = {
    dport => $dport,
    proto => $proto
  }

  if $source {

    # hash that contains the IP addresses
    $ips = profiles::firewall_hash_ips($name, $source)

    # declare a defined type for each of the rules in turn
    each ($ips) |$rule_name, $ip| {
      profiles::firewall::accept { $rule_name:
        source => $ip,
        *      => $defaults
      }
    }

  } else {

    profiles::firewall::accept { $name:
      * => $defaults
    }

  }
}
