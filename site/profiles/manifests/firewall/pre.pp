# == Class: profiles::firewall::pre
#
# Allows ICMP, loopback and established traffic.
#

class profiles::firewall::pre {

  # avoid dependency cycles
  Firewall {
    require => undef
  }

  # allow all ICMP packets
  firewall { '000 accept icmp':
    proto  => 'icmp',
    action => 'accept'
  }
  firewall { '000 accept icmp (IPv6)':
    proto    => 'icmp',
    action   => 'accept',
    provider => 'ip6tables'
  }

  # allow loopback traffic
  firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept'
  }
  firewall { '001 accept all to lo interface (IPv6)':
    proto    => 'all',
    iniface  => 'lo',
    action   => 'accept',
    provider => 'ip6tables'
  }

  # reject loopback traffic on public interfaces
  firewall { '002 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject'
  }
  firewall { '002 reject local traffic not on loopback interface (IPv6)':
    iniface     => '! lo',
    proto       => 'all',
    destination => '::1/128',
    action      => 'reject',
    provider    => 'ip6tables'
  }

  # allow traffic from established connections
  firewall { '003 accept related established traffic':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept'
  }
  firewall { '003 accept related established traffic (IPv6)':
    proto    => 'all',
    state    => ['RELATED', 'ESTABLISHED'],
    action   => 'accept',
    provider => 'ip6tables'
  }

}
