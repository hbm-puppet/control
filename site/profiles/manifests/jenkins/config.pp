# == Class: profiles::jenkins::config

class profiles::jenkins::config (
  Optional[Integer] $executors   = 4,
  Optional[Hash]    $environment = {},
) {

  # only create the config file if the jenkins_version custom fact is defined
  if $facts['jenkins_version'] {
    file { '/var/lib/jenkins/config.xml':
      ensure  => 'present',
      owner   => 'jenkins',
      group   => 'jenkins',
      mode    => '0644',
      content => epp('profiles/jenkins/config.xml.epp', {
        'version'     => $facts['jenkins_version'],
        'executors'   => $executors,
        'environment' => $environment,
        'length'      => length($environment)
      }),
      require => [
        User['jenkins'],
        Group['jenkins'],
        File['/var/lib/jenkins']
      ],
      notify  => Service['jenkins']
    }
  }

}