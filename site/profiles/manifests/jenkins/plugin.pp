# == Defined Type: profiles::jenkins::plugin

define profiles::jenkins::plugin () {

  # work out the cli arguments
  $jar  = '/usr/lib/jenkins/jenkins-cli.jar'
  $site = 'http://127.0.0.1:8080/'
  $auth   = '-ssh -user admin -i /etc/jenkins/api_id_rsa'

  # install the plugin
  exec { "profiles-jenkins-plugin-install-${name}":
    command => "/usr/bin/java -jar ${jar} -s ${site} ${auth} install-plugin ${name}",
    onlyif  => '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/',
    unless  => "/usr/bin/test -d /var/lib/jenkins/plugins/${name}",
    require => Exec['profiles-jenkins-unzip-jenkins-cli'],
    notify  => Service['jenkins']
  }

  # detect new versions and update - new available versions listed in brackets at end of line
  exec { "profiles-jenkins-plugin-update-${name}":
    command => "/usr/bin/java -jar ${jar} -s ${site} ${auth} install-plugin ${name}",
    onlyif  => [
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/',
      "/usr/bin/java -jar ${jar} -s ${site} ${auth} list-plugins ${name} | /usr/bin/grep \"(.*)$\""
    ],
    require => Exec['profiles-jenkins-unzip-jenkins-cli'],
    notify  => Service['jenkins']
  }
}
