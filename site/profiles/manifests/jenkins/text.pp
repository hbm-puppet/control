# == Defined Type: profiles::jenkins::text

define profiles::jenkins::text (
  String $uuid,
  String $content,
) {

  $xml = "/etc/jenkins/text/${name}.xml"

  # create the xml for creating the credential
  file { $xml:
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0600',
    content => epp('profiles/jenkins/text.xml.epp', {
      'uuid'    => $uuid,
      'name'    => $name,
      'content' => $content
    }),
    require => File['/etc/jenkins/text']
  }

  # work out the cli arguments
  $jar    = '/usr/lib/jenkins/jenkins-cli.jar'
  $site   = 'http://127.0.0.1:8080/'
  $auth   = '-ssh -user admin -i /etc/jenkins/api_id_rsa'
  $create = 'create-credentials-by-xml "SystemCredentialsProvider::SystemContextResolver::jenkins" "(global)"'
  $check  = "get-credentials-as-xml system::system::jenkins _ ${uuid}"

  # install the credentials for the first time
  exec { "profiles-jenkins-text-create-${name}":
    command => "/usr/bin/java -jar ${jar} -s ${site} ${auth} ${create} < ${xml}",
    unless  => "/usr/bin/java -jar ${jar} -s ${site} ${auth} ${check}",
    onlyif  => [
      '/usr/bin/test -d /var/lib/jenkins/plugins/credentials',
      '/usr/bin/curl -fsSI http://127.0.0.1:8080/cli/'
    ],
    require => File[$xml]
  }

}
