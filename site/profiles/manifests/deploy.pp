# == Class: profiles::deploy

class profiles::deploy () {

  # profile lookups
  $destination = lookup('profiles::deploy::file::destination', String)
  $private_key = lookup('profiles::deploy::private_key',       String)

  # create destination directory for downloaded files
  file { $destination:
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0700'
  }

  # install git
  include 'profiles::package::git'

  # ensure that the ssh directory exists
  file { '/usr/local/gitssh':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0700'
  }

  # install the private key
  file { '/usr/local/gitssh/id_rsa':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => $private_key,
    require => File['/usr/local/gitssh']
  }

  # install the SSH config file
  file { '/usr/local/gitssh/config':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/deploy/config',
    require => File['/usr/local/gitssh']
  }

  # create the known_hosts file
  file { '/usr/local/gitssh/known_hosts':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/deploy/known_hosts',
    require => File['/usr/local/gitssh']
  }

  # create an SSH wrapper script that will use the config file
  file { '/usr/local/gitssh/gitssh.sh':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/profiles/deploy/gitssh.sh',
    require => File['/usr/local/gitssh']
  }

  # create an script for checking for remote updates
  file { '/usr/local/gitssh/checkforupdates.sh':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/profiles/deploy/checkforupdates.sh',
    require => File['/usr/local/gitssh']
  }

}
