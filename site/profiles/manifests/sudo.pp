# == Class: profiles::sudo

class profiles::sudo () {

  # make sure that sudo is installed
  package{ 'sudo':
    ensure => 'present'
  }

  # install the sudoers file
  file { '/etc/sudoers':
    ensure => 'present',
    source => 'puppet:///modules/profiles/sudo/sudoers',
    owner  => 'root',
    group  => 'root',
    mode   => '0440'
  }

  # purge the sudoers.d directory of non-puppet controlled files
  file { '/etc/sudoers.d':
    ensure  => 'directory',
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    recurse => true,
    purge   => true
  }

}
