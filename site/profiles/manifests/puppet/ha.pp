# == Class: profiles::puppet::ha

class profiles::puppet::ha () {

  # look up the lists of puppet nodes and servers
  $puppet_nodes   = lookup('profiles::puppet::nodes',   Array[String])
  $puppet_servers = lookup('profiles::puppet::servers', Array[String])

  # open the firewall
  profiles::firewall::rule { 'HTTPS to puppet server':
    dport  => 8140,
    proto  => 'tcp',
    source => $puppet_nodes
  }

  # install haproxy
  class { 'haproxy':
    merge_options => true
  }

  # configure listener for puppetserver
  haproxy::listen { 'puppetserver':
    ipaddress        => '0.0.0.0',
    ports            => '8140',
    options          => {
      'mode'   => 'tcp',
      'option' => [ 'ssl-hello-chk' ]
    },
    collect_exported => false
  }

  # configure each of the members
  each ($puppet_servers) |$index, $fqdn| {
    haproxy::balancermember { $fqdn:
      listening_service => 'puppetserver',
      server_names      => $fqdn,
      ipaddresses       => profiles::resolve($fqdn),
      ports             => '8140',
      options           => 'check'
    }
  }

}