# == Class: profiles::puppet::puppetdb

class profiles::puppet::puppetdb () {

  # profile lookups
  $password = lookup('profiles::puppet::puppetdb::password', String)

  # look up the lists of puppet servers and puppetboard servers
  $puppet_servers      = lookup('profiles::puppet::servers',             Array[String])
  $puppetboard_servers = lookup('profiles::puppet::puppetboard_servers', Array[String])

  # look up the puppetdb port
  $puppetdb_port = lookup('profiles::puppet::puppetdb_port', Integer)

  # open the firewall for puppet servers
  profiles::firewall::rule { 'PuppetDB from Puppet servers':
    dport  => $puppetdb_port,
    proto  => 'tcp',
    source => $puppet_servers
  }

  # open the firewall for puppetboard servers
  profiles::firewall::rule { 'PuppetDB from Puppetboard':
    dport  => $puppetdb_port,
    proto  => 'tcp',
    source => $puppetboard_servers
  }

  # install git
  include 'profiles::package::git'

  # create puppetdb user
  profiles::accounts::functional { 'puppetdb':
    home    => '/opt/puppetlabs/server/data/puppetdb',
    comment => 'puppetdb daemon'
  }

  # copy the Puppet private key for puppetdb
  file { "/etc/pki/tls/private/${trusted['certname']}-puppetdb.pem":
    ensure  => 'present',
    owner   => 'puppetdb',
    group   => 'puppetdb',
    mode    => '0400',
    source  => "/etc/puppetlabs/puppet/ssl/private_keys/${trusted['certname']}.pem",
    require => [
      User['puppetdb'],
      Group['puppetdb']
    ]
  }

  # configure the puppetdb database
  class { 'puppetdb::database::postgresql':
    listen_addresses  => '127.0.0.1',
    database_password => $password
  }

  # configure the puppetdb application
  class { 'puppetdb::server':
    puppetdb_user      => 'puppetdb',
    puppetdb_group     => 'puppetdb',
    database_host      => '127.0.0.1',
    database_password  => $password,
    disable_cleartext  => true,
    ssl_set_cert_paths => true,
    ssl_key_path       => "/etc/pki/tls/private/${trusted['certname']}-puppetdb.pem",
    ssl_cert_path      => "/etc/puppetlabs/puppet/ssl/certs/${trusted['certname']}.pem",
    ssl_ca_cert_path   => '/etc/puppetlabs/puppet/ssl/certs/ca.pem',
    ssl_listen_port    => $puppetdb_port,
    require            => [
      User['puppetdb'],
      Group['puppetdb'],
      Class['puppetdb::database::postgresql'],
      File["/etc/pki/tls/private/${trusted['certname']}-puppetdb.pem"]
    ]
  }

}
