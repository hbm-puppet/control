# == Class: profiles::puppet::puppetboard

class profiles::puppet::puppetboard () {

  # profile lookups
  $repo              = lookup('profiles::puppet::puppetboard::repo',              String)
  $release           = lookup('profiles::puppet::puppetboard::release',           String)
  $python_version    = lookup('profiles::puppet::puppetboard::python_version',    String)
  $ssl_key           = lookup('profiles::puppet::puppetboard::ssl_key',           Optional[String],  'first', undef)
  $ssl_cert          = lookup('profiles::puppet::puppetboard::ssl_cert',          Optional[String],  'first', undef)
  $ssl_chaining_cert = lookup('profiles::puppet::puppetboard::ssl_chaining_cert', Optional[String],  'first', undef)
  $gunicorn_port     = lookup('profiles::puppet::puppetboard::gunicorn_port',     Optional[Integer], 'first', 9080)

  # look up the puppetdb server and port
  $puppetdb_server = lookup('profiles::puppet::puppetdb_server', String)
  $puppetdb_port   = lookup('profiles::puppet::puppetdb_port',   Integer)

  # lookup the list of admin pcs
  $admin_pcs = lookup('profiles::admin::pcs', Array[String], 'first', [])

  # allow admin pcs to access puppetboard
  profiles::firewall::rule { 'HTTPS from admin PCs':
    dport  => 443,
    proto  => 'tcp',
    source => $admin_pcs
  }

  # install python
  class { 'profiles::puppet::python':
    version => $python_version
  }

  # create puppetboard user
  profiles::accounts::functional { 'puppetboard':
    groups => [ 'gunicorn' ]
  }

  # install apache
  include 'profiles::apache'

  # install mod_proxy
  include 'apache::mod::proxy'

  # work out the paths for the key and certificate
  if $ssl_key {
    $ssl_key_path = $ssl_key
  } else {
    $ssl_key_path = "/etc/puppetlabs/puppet/ssl/private_keys/${trusted['certname']}.pem"
  }
  if $ssl_cert {
    $ssl_cert_path = $ssl_cert
  } else {
    $ssl_cert_path = "/etc/puppetlabs/puppet/ssl/certs/${trusted['certname']}.pem"
  }

  # configure the vhost for puppetboard
  apache::vhost { "${trusted['certname']}-443":
    servername  => $trusted['certname'],
    port        => 443,
    ssl         => true,
    ssl_key     => $ssl_key_path,
    ssl_cert    => $ssl_cert_path,
    ssl_chain   => $ssl_chaining_cert,
    docroot     => '/var/www/html',
    directories => [
      {
        path           => '/var/www/html',
        auth_require   => 'all granted',
        allow_override => 'None'
      }
    ],
    proxy_pass  => [
      {
        'path' => '/',
        'url'  => "http://127.0.0.1:${gunicorn_port}/"
      }
    ]
  }

  # make a copy of the Puppet node private key and certificate for puppetboard
  file { "/etc/pki/tls/private/${trusted['certname']}-puppetboard.pem":
    ensure  => 'present',
    owner   => 'puppetboard',
    group   => 'puppetboard',
    mode    => '0400',
    source  => "/etc/puppetlabs/puppet/ssl/private_keys/${trusted['certname']}.pem",
    require => [
      User['puppetboard'],
      Group['puppetboard']
    ]
  }
  file { "/etc/pki/tls/certs/${trusted['certname']}-puppetboard.pem":
    ensure  => 'present',
    owner   => 'puppetboard',
    group   => 'puppetboard',
    mode    => '0444',
    source  => "/etc/puppetlabs/puppet/ssl/certs/${trusted['certname']}.pem",
    require => [
      User['puppetboard'],
      Group['puppetboard']
    ]
  }

  # deploy puppetboard
  profiles::deploy::git { 'puppetboard':
    repo       => $repo,
    release    => $release,
    deploy_dir => '/apps/puppetboard'
  }

  # deploy puppetboard settings
  file { '/apps/puppetboard/settings.py':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profiles/puppet/puppetboard/settings.py.epp', {
      cert            => "/etc/pki/tls/certs/${trusted['certname']}-puppetboard.pem",
      private_key     => "/etc/pki/tls/private/${trusted['certname']}-puppetboard.pem",
      puppetdb_server => $puppetdb_server,
      puppetdb_port   => $puppetdb_port
    }),
    require => [
      Profiles::Deploy::Git['puppetboard'],
      File["/etc/pki/tls/certs/${trusted['certname']}-puppetboard.pem"],
      File["/etc/pki/tls/private/${trusted['certname']}-puppetboard.pem"]
    ]
  }

  # set up gunicorn for puppetboard
  profiles::puppet::gunicorn { 'puppetboard':
    appdir         => '/apps/puppetboard',
    module         => 'puppetboard.app:app',
    requirements   => '/apps/puppetboard/requirements.txt',
    environment    => {
      'PUPPETBOARD_SETTINGS' => '/apps/puppetboard/settings.py'
    },
    owner          => 'puppetboard',
    group          => 'puppetboard',
    port           => $gunicorn_port,
    python_version => $python_version,
    require        => [
      Profiles::Deploy::Git['puppetboard'],
      File['/apps/puppetboard/settings.py'],
    ]
  }

}
