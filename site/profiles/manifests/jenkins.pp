# == profiles::jenkins

class profiles::jenkins () {

  # profile lookups
  $admin_password      = lookup('profiles::jenkins::admin_password',      String)
  $admin_password_salt = lookup('profiles::jenkins::admin_password_salt', String)
  $api_ssh_key         = lookup('profiles::jenkins::api_ssh_key',         String)
  $api_ssh_pubkey      = lookup('profiles::jenkins::api_ssh_pubkey',      String)
  $hipchat_room        = lookup('profiles::jenkins::hipchat_room',        Optional[String],  'first', undef)
  $hipchat_token       = lookup('profiles::jenkins::hipchat_token',       Optional[String],  'first', undef)
  $hipchat_token_uuid  = lookup('profiles::jenkins::hipchat_token_uuid',  Optional[String],  'first', undef)
  $ssl_key             = lookup('profiles::jenkins::ssl_key',             Optional[String],  'first', undef)
  $ssl_cert            = lookup('profiles::jenkins::ssl_cert',            Optional[String],  'first', undef)
  $ssl_chaining_cert   = lookup('profiles::jenkins::ssl_chaining_cert',   Optional[String],  'first', undef)

  # look up the list of admin pcs
  $admin_pcs = lookup('profiles::admin::pcs', Array, 'first', [])

  # allow admin pcs to access jenkins
  profiles::firewall::rule { 'HTTPS from admin PCs':
    dport  => 443,
    proto  => 'tcp',
    source => $admin_pcs
  }

  # install prerequisite packages
  include 'profiles::package::ant'
  include 'profiles::package::git'
  include 'profiles::package::openjdk'
  include 'profiles::package::unzip'

  # install the Jenkins LTS repo
  include 'profiles::repo::jenkins'

  # create jenkins user
  profiles::accounts::functional { 'jenkins': }

  # install apache
  include 'profiles::apache'

  # install mod_proxy
  include 'apache::mod::proxy'

  # work out the paths for the key and certificate
  if $ssl_key {
    $ssl_key_path = $ssl_key
  } else {
    $ssl_key_path = "/etc/puppetlabs/puppet/ssl/private_keys/${trusted['certname']}.pem"
  }
  if $ssl_cert {
    $ssl_cert_path = $ssl_cert
  } else {
    $ssl_cert_path = "/etc/puppetlabs/puppet/ssl/certs/${trusted['certname']}.pem"
  }

  # define the vhost
  apache::vhost { "${trusted['certname']}-443":
    servername      => $trusted['certname'],
    port            => 443,
    ssl             => true,
    ssl_key         => $ssl_key_path,
    ssl_cert        => $ssl_cert_path,
    ssl_chain       => $ssl_chaining_cert,
    docroot         => '/var/www/html',
    directories     => [
      {
        path           => '/var/www/html',
        auth_require   => 'all granted',
        allow_override => 'None'
      }
    ],
    request_headers => [
      'set X-Forwarded-Proto "https"',
      "set X-Forwarded-Host \"${trusted['certname']}\"",
      'set X-Forwarded-Port "443"'
    ],
    proxy_pass      => [
      {
        'path' => '/',
        'url'  => 'http://localhost:8080/'
      }
    ]
  }

  # create directory structure 
  file { [
    '/etc/jenkins',
    '/etc/jenkins/jobs',
    '/etc/jenkins/pipelines',
    '/var/lib/jenkins',
    '/var/lib/jenkins/users',
    '/var/lib/jenkins/users/admin'
  ]:
    ensure => 'directory',
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0644'
  }
  file { [
    '/etc/jenkins/keys',
    '/etc/jenkins/files',
    '/etc/jenkins/text',
    '/var/cache/r10k'
  ]:
    ensure => 'directory',
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0600'
  }

  # install jenkins
  package { 'jenkins':
    ensure  => 'latest',
    require => [
      Yumrepo['jenkins'],
      User['jenkins'],
      Group['jenkins'],
      File['/var/lib/jenkins']
    ]
  }

  # disable remoting CLI
  file { '/var/lib/jenkins/jenkins.CLI.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/jenkins/jenkins.CLI.xml',
    require => Package['jenkins'],
    notify  => Service['jenkins']
  }

  # enable ssh CLI
  file { '/var/lib/jenkins/org.jenkinsci.main.modules.sshd.SSHD.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/jenkins/org.jenkinsci.main.modules.sshd.SSHD.xml',
    require => Package['jenkins'],
    notify  => Service['jenkins']
  }

  # disable security warnings
  file { '/var/lib/jenkins/jenkins.security.UpdateSiteWarningsConfiguration.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/jenkins/jenkins.security.UpdateSiteWarningsConfiguration.xml',
    require => Package['jenkins'],
    notify  => Service['jenkins']
  }
  file { '/var/lib/jenkins/jenkins.security.QueueItemAuthenticatorConfiguration.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    source  => 'puppet:///modules/profiles/jenkins/jenkins.security.QueueItemAuthenticatorConfiguration.xml',
    require => Package['jenkins'],
    notify  => Service['jenkins']
  }

  # set url and admin email
  file { '/var/lib/jenkins/jenkins.model.JenkinsLocationConfiguration.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    content => epp('profiles/jenkins/jenkins.model.JenkinsLocationConfiguration.xml.epp', {
      'url'   => "https://${trusted['certname']}/",
      'email' => 'root@localhost'
    }),
    require => Package['jenkins'],
    notify  => Service['jenkins']
  }

  # create api private key
  file { '/etc/jenkins/api_id_rsa':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0600',
    content => $api_ssh_key,
    require => File['/etc/jenkins']
  }

  # create admin user
  file { '/var/lib/jenkins/users/admin/config.xml':
    ensure  => 'present',
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    content => epp('profiles/jenkins/admin-config.xml.epp', {
      'password' => profiles::jenkins_bcrypt($admin_password, $admin_password_salt),
      'pubkey'   => $api_ssh_pubkey
    }),
    require => File['/var/lib/jenkins/users/admin']
  }

  # extract jenkins-cli.jar
  exec { 'profiles-jenkins-unzip-jenkins-cli':
    command => '/usr/bin/unzip -j /usr/lib/jenkins/jenkins.war -d /usr/lib/jenkins WEB-INF/jenkins-cli.jar',
    creates => '/usr/lib/jenkins/jenkins-cli.jar',
    require => [
      Package['jenkins'],
      Package['unzip']
    ]
  }

  # install the standard list of plugins
  profiles::jenkins::plugin { [
    'ace-editor',
    'ant',
    'apache-httpcomponents-client-4-api',
    'bouncycastle-api',
    'branch-api',
    'build-timeout',
    'cloudbees-folder',
    'command-launcher',
    'config-file-provider',
    'credentials',
    'credentials-binding',
    'display-url-api',
    'durable-task',
    'git',
    'git-client',
    'jdk-tool',
    'jquery-detached',
    'jsch',
    'junit',
    'mailer',
    'matrix-project',
    'pipeline-multibranch-defaults',
    'plain-credentials',
    'resource-disposer',
    'scm-api',
    'script-security',
    'ssh-credentials',
    'structs',
    'timestamper',
    'token-macro',
    'workflow-api',
    'workflow-cps',
    'workflow-durable-task-step',
    'workflow-job',
    'workflow-multibranch',
    'workflow-scm-step',
    'workflow-step-api',
    'workflow-support',
    'ws-cleanup'
  ]: }

  if $hipchat_room and $hipchat_token and $hipchat_token_uuid {

    # install the hipchat plugin
    profiles::jenkins::plugin { 'hipchat': }

    # store the HipChat token
    profiles::jenkins::text { 'HIPCHAT_TOKEN':
      uuid    => $hipchat_token_uuid,
      content => $hipchat_token
    }

    # configure HipChat
    file { '/var/lib/jenkins/jenkins.plugins.hipchat.HipChatNotifier.xml':
      ensure  => 'present',
      owner   => 'jenkins',
      group   => 'jenkins',
      mode    => '0644',
      content => epp('profiles/jenkins/jenkins.plugins.hipchat.HipChatNotifier.xml.epp', {
        'room' => $hipchat_room,
        'uuid' => $hipchat_token_uuid
      }),
      require => Package['jenkins'],
      notify  => Service['jenkins']
    }

  }

  # ensure that jenkins is running
  service { 'jenkins':
    ensure     => 'running',
    enable     => true,
    hasrestart => true
  }

}