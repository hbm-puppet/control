# == Class: profiles::accounts

class profiles::accounts () {

  # profile lookups
  $groups      = lookup('profiles::accounts::groups',      Array, 'unique', [])
  $users       = lookup('profiles::accounts::users',       Hash,  'hash',   {})
  $functionals = lookup('profiles::accounts::functionals', Hash,  'hash',   {})

  # create groups
  each($groups) |$index, $name| {
    profiles::accounts::group { $name: }
  }

  # create user accounts
  each($users) |$name, $parameters| {
    profiles::accounts::user { $name:
      * => $parameters
    }
  }

  # create functional accounts
  each($functionals) |$name, $parameters| {
    profiles::accounts::functional { $name:
      * => $parameters
    }
  }

}
