# == Class: profiles::selinux

class profiles::selinux (
  String $state,
) {

  # set the desired state at next boot
  file { '/etc/selinux/config':
    ensure  => 'present',
    content => epp('profiles/selinux/config.epp',
      {
        'state' => $state
      }
    ),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Exec['selinux-setenforce']
  }

  # update state of running system if it has changed
  exec { 'selinux-setenforce':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => "setenforce ${state}",
    refreshonly => true
  }

}
