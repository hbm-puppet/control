# == Defined Type: profiles::deploy::git

define profiles::deploy::git (
  String $repo,
  String $release,
  String $deploy_dir
) {

  # configure git
  include 'profiles::deploy'

  # set the git ssh command in the environment
  $environment = 'GIT_SSH=/usr/local/gitssh/gitssh.sh'

  # set up the repo if it does not exist
  file { $deploy_dir:
    ensure => 'directory',
    notify => Exec["git-init-${name}"]
  }

  # initialize the repo
  exec { "git-init-${name}":
    cwd         => $deploy_dir,
    environment => $environment,
    command     => '/usr/bin/git init',
    refreshonly => true,
    require     => File['/usr/local/gitssh/known_hosts'],
    notify      => Exec["git-remote-${name}"]
  }

  # set the remote
  exec { "git-remote-${name}":
    cwd         => $deploy_dir,
    environment => $environment,
    command     => "/usr/bin/git remote add origin ${repo}",
    refreshonly => true
  }

  # checkout the release if the local repo is not on that release
  exec { "git-checkout-branch-${name}":
    cwd         => $deploy_dir,
    environment => $environment,
    command     => "/usr/bin/git fetch && /usr/bin/git checkout ${release}",
    unless      => [
      "/usr/bin/git branch | /usr/bin/grep \"* ${release}\"",
      "/usr/bin/git ls-remote -t origin refs/tags/${release} | /usr/bin/grep ${release}"
    ],
    require     => File['/usr/local/gitssh/known_hosts']
  }

  exec { "git-checkout-tag-${name}":
    cwd         => $deploy_dir,
    environment => $environment,
    command     => "/usr/bin/git fetch && /usr/bin/git checkout ${release}",
    unless      => "/usr/bin/git branch | /usr/bin/grep \"* (detached from ${release})\"",
    onlyif      => "/usr/bin/git ls-remote -t origin refs/tags/${release} | /usr/bin/grep ${release}",
    require     => File['/usr/local/gitssh/known_hosts']
  }

  # perform a /usr/bin/git pull to get the branch up to date if required
  exec { "git-pull-${name}":
    cwd         => $deploy_dir,
    environment => $environment,
    command     => '/usr/bin/git pull',
    unless      => "/usr/local/gitssh/checkforupdates.sh ${release}",
    require     => [
      Exec["git-checkout-branch-${name}"],
      Exec["git-checkout-tag-${name}"]
    ]
  }

}
