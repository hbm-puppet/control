# == Class: profiles::certs

class profiles::certs () {

  # profile lookups
  $certs        = lookup('profiles::certs::certs',        Hash, 'hash', {})
  $private_keys = lookup('profiles::certs::private_keys', Hash, 'hash', {})
  $ca_certs     = lookup('profiles::certs::ca_certs',     Hash, 'hash', {})

  # create the certs
  each($certs) |$name, $parameters| {
    file { "/etc/pki/tls/certs/${name}.crt":
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      *      => $parameters
    }
  }

  # create the private keys
  each($private_keys) |$name, $parameters| {
    file { "/etc/pki/tls/private/${name}.key":
      ensure    => 'present',
      owner     => 'root',
      group     => 'root',
      mode      => '0400',
      show_diff => false,
      *         => $parameters
    }
  }

  # create the CA certificates
  each($ca_certs) |$name, $parameters| {
    file { "/etc/pki/CA/certs/${name}.crt":
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      notify => Exec['profiles_certs_cacertdir_rehash'],
      *      => $parameters
    }
    file { "/etc/pki/ca-trust/source/anchors/${name}.crt":
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      notify => Exec['profiles_certs_update_ca_trust'],
      *      => $parameters
    }
  }

  # trust the Puppet CA certificate as a root certificate
  file { "/etc/pki/CA/certs/puppetca.crt":
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
    source => '/etc/puppetlabs/puppet/ssl/certs/ca.pem',
    notify => Exec['profiles_certs_cacertdir_rehash']
  }
  file { '/etc/pki/ca-trust/source/anchors/puppetca.crt':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
    source => '/etc/puppetlabs/puppet/ssl/certs/ca.pem',
    notify => Exec['profiles_certs_update_ca_trust']
  }

  # rehash the CA certificates
  exec { 'profiles_certs_cacertdir_rehash':
    command     => '/usr/sbin/cacertdir_rehash /etc/pki/CA/certs/',
    refreshonly => true
  }

  # update the list of trusted CA certificates
  exec { 'profiles_certs_update_ca_trust':
    command     => '/usr/bin/update-ca-trust',
    refreshonly => true
  }

}
