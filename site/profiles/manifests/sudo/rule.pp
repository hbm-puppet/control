# == Class: profiles::sudo::rule

define profiles::sudo::rule (
  String            $as,
  Array[String]     $commands,
  Optional[String]  $user      = $name,
  Optional[String]  $group     = undef,
  Optional[Boolean] $nopasswd  = false,
  Optional[Boolean] $setenv    = false
) {

  # work out who the rule is for
  if $group {
    $for = "%${group}"
  } else {
    $for = $user
  }

  # work out the name of the rule file
  $rule_file = "/etc/sudoers.d/10_${name}"

  # create the rule file
  concat { $rule_file:
    ensure  => 'present',
    require => File['/etc/sudoers.d']
  }

  # add fragments to the concat for each command
  each($commands) |$index, $command| {
    profiles::sudo::fragment { "${name}_${index}":
      command  => $command,
      as       => $as,
      for      => $for,
      nopasswd => $nopasswd,
      setenv   => $setenv,
      target   => $rule_file,
      order    => $index,
    }
  }

}
