# == Class: profiles::sudo::fragment

define profiles::sudo::fragment (
  String  $command,
  String  $as,
  String  $for,
  Boolean $nopasswd,
  Boolean $setenv,
  String  $target,
  Integer $order,
) {

  # create the authorization part of the rule
  if $nopasswd {
    $nopasswd_string = ' NOPASSWD:'
  } else {
    $nopasswd_string = ''
  }
  if $setenv {
    $setenv_string = ' SETENV:'
  } else {
    $setenv_string = ''
  }
  $auth = "${for} ALL=(${as})${nopasswd_string}${setenv_string}"

  # create the concat fragment
  concat::fragment { $name:
    target  => $target,
    content => "${auth} ${command}\n",
    order   => $order
  }

}
