# == Class: profiles::apache

class profiles::apache () {

  # profile lookups
  $log_format = lookup('profiles::apache::log_format', String)

  # install and configure Apache
  class { 'apache':
    log_formats   => {
      combined => $log_format
    },
    logroot_mode  => '0755',
    purge_configs => false
  }

  # install SSL by default
  include 'apache::mod::ssl'

}
