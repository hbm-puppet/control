# == Class: profiles::repo::epel

class profiles::repo::epel () {

  case $facts['os']['name'] {

    'CentOS': {
      package { 'epel-release': ensure => 'present' }
    }

    default: {
      fail("Class profiles::repos::epel is not supported on ${facts['os']['name']}")
    }

  }

}
