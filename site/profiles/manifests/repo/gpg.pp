# == Type: profiles::repo::gpg

define profiles::repo::gpg () {

  $gpg_path = "/etc/pki/rpm-gpg/RPM-GPG-KEY-${name}"

  # add gpg key
  file { $gpg_path:
    ensure => 'present',
    source => "puppet:///modules/profiles/repo/RPM-GPG-KEY-${name}",
    owner  => 'root',
    group  => 'root',
    mode   => '0644'
  }

  # import gpg key
  exec { "import-RPM-GPG-KEY-${name}":
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => "rpm --import ${gpg_path}",
    unless  => "rpm -q gpg-pubkey-$(gpg --throw-keyids ${gpg_path} | awk -F'[/ ]+' '/^pub/ {print tolower(\$3)}')",
    require => File[$gpg_path]
  }

}
