# == Class: profiles::package::git

class profiles::package::git () {

  package { 'git':
    ensure => 'present'
  }

}
