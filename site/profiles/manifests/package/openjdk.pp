# == Class: profiles::package::openjdk

class profiles::package::openjdk () {

  package { 'java-1.8.0-openjdk':
    ensure => 'present'
  }

}
