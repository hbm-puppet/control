# == Class: profiles::package::ant

class profiles::package::ant () {

  package { [ 'ant', 'ant-contrib', 'ant-jsch' ]:
    ensure => 'present'
  }

}
