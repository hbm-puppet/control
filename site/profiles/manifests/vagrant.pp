# == Class: profiles::vagrant

class profiles::vagrant () {

  # open the firewall to allow LLMNR queries
  profiles::firewall::rule { 'LLMNR':
    dport => 5535,
    proto => 'udp'
  }

  # catch-all sudo rule for vagrant user
  profiles::sudo::rule { 'vagrant_all':
    user     => 'vagrant',
    as       => 'ALL',
    nopasswd => true,
    commands => [ 'ALL' ]
  }

}
