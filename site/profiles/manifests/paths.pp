# == Class: profiles::paths

class profiles::paths () {

  # declare standard paths
  file { [
    '/apps',
    '/srv'
  ]:
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755'
  }

}
