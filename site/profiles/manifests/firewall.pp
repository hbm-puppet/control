# == Class: profiles::firewall

class profiles::firewall {

  # include Puppetlabs firewall module
  include 'firewall'

  # purge existing firewall rules
  resources { 'firewall':
    purge => true
  }

  # Load the pre and post rules
  include 'profiles::firewall::pre'
  include 'profiles::firewall::post'

  # ensure that they are added first and last
  Firewall {
    require => Class['profiles::firewall::pre'],
    before  => Class['profiles::firewall::post']
  }

}
