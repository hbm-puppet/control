#
# Returns a hash structure suitable for creating defined types
#
Puppet::Functions.create_function(:'profiles::paths_parent_paths') do

  dispatch :paths_parent_paths_string do
    required_param 'String', :path
    return_type    'Array'
  end

  def paths_parent_paths_string(path)
    # split the path into individual folder names
    folder_names = path.gsub(/^\//, '\1').split('/')
    parents = []
    # gsub creates empty item for / so there can only be a parent if there
    # are three or more folder names (e.g. / + parent/ + current/)
    if folder_names.length > 2 then
      # get rid of empty item created by the gsub for /
      folder_names.pop
      parent = ""
      folder_names.each do |folder_name|
        # grow the parent path by appending the next folder name
        parent = parent + "/#{folder_name}"
        # copy the parent path into the array
        parents.push(path)
      end
    end
    return parents
  end

end