#
# Returns a bcrypt hash of the given password. If bcrypt is not available
# a temporary password hash is returned instead. This occurs when Jenkins is
# being provisioned manually. The next Puppet agent run will set the password
# as specified.
#
Puppet::Functions.create_function(:'profiles::jenkins_bcrypt') do

  dispatch :jenkins_bcrypt_string do
    required_param 'String', :password
    return_type    'String'
  end

  dispatch :jenkins_bcrypt_string_string do
    required_param 'String', :password
    required_param 'String', :salt
    return_type    'String'
  end

  def jenkins_bcrypt_string(password)
    begin
      require 'bcrypt'
      return BCrypt::Password.create(password)
    rescue LoadError
      return '$2a$10$X7olh.Ne/kKn0ZSstr3swuMvofHZBlKqzescA/a4RbHg4iFqoiCDy'
    end
  end

  def jenkins_bcrypt_string_string(password, salt)
    begin
      require 'bcrypt'
      return BCrypt::Engine.hash_secret(password, salt)
    rescue LoadError
      return '$2a$10$X7olh.Ne/kKn0ZSstr3swuMvofHZBlKqzescA/a4RbHg4iFqoiCDy'
    end
  end
end
