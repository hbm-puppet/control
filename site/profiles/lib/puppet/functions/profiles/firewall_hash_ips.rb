#
# Returns a hash structure suitable for defining profiles::firewall_accept
# defined types.
#
Puppet::Functions.create_function(:'profiles::firewall_hash_ips') do

  #
  # Creates a hash structure mapping a rule name string to a hash
  # containing a reference to a single IP address or IP address range.
  # This is used to declare a profiles::firewall_accept defined type.
  #
  # Calling this function like so:
  #
  #   profiles::firewall_hash_ips("example", "129.215.0.0/16")
  #
  # Will return the following structure:
  # 
  #   {
  #     "example from 129.215.0.0/16" => { ip => "129.215.0.0/16" }
  #   }
  #
  # Which will be used to effectively declare a profiles::firewall_accept
  # defined type like so:
  #
  #   profiles::firewall::accept { "example (129.215.0.0/16)":
  #     ip => "129.215.0.0/16"
  #   }
  #
  # @param  [String] rule_name String describing the firewall rule.
  # @parama [String] ip        String containing the IP address or IP
  #                            address range.
  # @return [Hash]
  #
  dispatch :hash_ips_string do
    required_param 'String', :rule_name
    required_param 'String', :ips
    return_type    'Hash'
  end

  #
  # Creates a hash structure containing a number of mappings that map
  # a rule name string to a hash containing a reference to a single IP
  # address or IP address range. The hash will contain a single mapping
  # for each IP address or IP address range contained in the array. See
  #
  # Calling this function like so:
  #
  #   profiles::firewall_hash_ips("example", [
  #     "172.16.0.0/16",
  #     "192.168.0.0/16"
  #   ])
  #
  # Will return the following structure:
  # 
  #   {
  #     "example from 172.16.0.0/16" => { ip => "172.16.0.0/16" },
  #     "example from 192.168.0.0/16" => { ip => "192.168.0.0/16" }
  #   }
  #
  # Which will be used to effectively declare a number of
  # profiles::firewall_accept defined types like so:
  #
  #   profiles::firewall::accept { "example (172.16.0.0/16)":
  #     ip => "172.16.0.0/16"
  #   }
  #   profiles::firewall::accept { "example (192.168.0.0/16)":
  #     ip => "192.168.0.0/16"
  #   }
  #
  # @param  [String] rule_name String describing the firewall rule.
  # @param  [Array]  ips       Array of strings containing the IP addresses
  #                            or IP address ranges.
  # @return [Hash]
  #
  dispatch :hash_ips_array do
    required_param 'String',        :rule_name
    required_param 'Array[String]', :ips
    return_type    'Hash'
  end

  def hash_ips_string(rule_name, ip)
    rule_hash = Hash.new
    rule_hash["%s (%s)" % [ rule_name, ip ]] = ip
    return rule_hash
  end

  def hash_ips_array(rule_name, ips)
    rule_hash = Hash.new
    ips.each do |ip|
      rule_hash["%s (%s)" % [ rule_name, ip ]] = ip
    end
    return rule_hash
  end

end
